#!/usr/bin/env perl

###########################################################################
#                                                                         #
# perl 5.10.0                                                             #
# REPAIN Paul, 2018                                                       #
# GitHub : @Poulpy                                                        #
# GitLab : @Poulposaure                                                   #
#                                                                         #
#                                                                         #
#                                                                         #
###########################################################################
#__________      .__                                               _____  #
#\______   \__ __|__| ______ ___________    ____   ____  ____     /  |  | #
# |     ___/  |  \  |/  ___//  ___/\__  \  /    \_/ ___\/ __ \   /   |  |_#
# |    |   |  |  /  |\___ \ \___ \  / __ \|   |  \  \__\  ___/  /    ^   /#
# |____|   |____/|__/____  >____  >(____  /___|  /\___  >___  > \____   | #
#                        \/     \/      \/     \/     \/    \/       |__| #
###########################################################################


#######################
#                     #
#       PAQUETS       #
#                     #
#######################


use Switch;
use strict;
use warnings;
use feature qw(say);
use Term::ANSIColor qw(:constants);


#######################
#                     #
# VARIABLES  GLOBALES #
#    ET CONSTANTES    #
#                     #
#######################


my $hauteur = 6;
my $longueur = 7;
my $nbTours = $hauteur * $longueur;
$Term::ANSIColor::AUTORESET = 1;

my %J = (0 => 'O',
         1 => 'X');
my $row1;
my $row2;
my $row3;
my $row4;
my $row5;
my $row6;
my @grille;
my $tempsImparti = 240;# durée d'une partie en secondes (4 minutes)


#######################
#                     #
#   PROTOTYPES  DES   #
#      FONCTIONS      #
#                     #
#######################


sub main();
sub initialiseGrille();
sub joue($);
sub afficheGrille($);
sub joueTour($$);
sub reponsePossible($);
sub caseVide($);
sub coordonneesCorrectes($);
sub victoire($$$);
sub clear();


#######################
#                     #
#      FONCTIONS      #
#                     #
#######################


sub main()
{
    while (1)
    {
        clear();
        say "__________      .__                                               _____  ";
        say "\\______   \\__ __|__| ______ ___________    ____   ____  ____     /  |  | ";
        say " |     ___/  |  \\  |/  ___//  ___/\\__  \\  /    \\_/ ___\\/ __ \\   /   |  |_";
        say " |    |   |  |  /  |\\___ \\ \\___ \\  / __ \\|   |  \\  \\__\\  ___/  /    ^   /";
        say " |____|   |____/|__/____  >____  >(____  /___|  /\\___  >___  > \\____   | ";
        say "                        \\/     \\/      \\/     \\/     \\/    \\/       |__| \n";
        say ". " x 37 . "\n";
        say " 1 - Joueur VS Joueur\n";
        say " 2 - Joueur VS Ordinateur\n";
        say " 3 - Configurations\n";
        print " 0 - Quitter\n\n-> ";
        my $choix = <>;
        chomp $choix;
        switch ($choix)
        {
            case '1'
            {
                joue(2);# joueur contre joueur
            }
            case '2'
            {
                joue(1);# joueur contre ordinateur
            }
            case '3'
            {
                configurations();
            }
            case '0'
            {
                say "\nMerci d'avoir joue !";
                return;# arret du programme
            }
        }# switch
    }# while
}# main

sub joue($)
{
    my ($humain) = @_;
    initialiseGrille();# remplit la grille de vide (sans jetons)
    my $debut = time();
    for (my $joueur = 0, my $nb_tours_actuels = 0 ; $nb_tours_actuels < $nbTours ; $joueur++, $nb_tours_actuels++)
    {
        clear();
        $joueur = $joueur%2;
        say "[Tour $nb_tours_actuels]\n\n";
        afficheGrille($joueur);
        say "Tour du joueur $joueur\n";
        my $statut = joueTour($joueur, $humain);
        my $fin = time();
        say "Temps ecoule !" and last if (time() - $debut > $tempsImparti); 
        if ($statut == 1)
        {
            say "Le joueur $joueur a gagne !\nJeu termine en " . ($fin - $debut) . " secondes";
            last;
        }
    say "Egalite...\n" if ($nb_tours_actuels == $nbTours - 1);
    }
    say "Rejouer [O/o/N/n] ? ";
    my $saisie = <>;
    chomp $saisie;
    joue($humain) if ($saisie eq 'O' or $saisie eq 'o');
}

sub configurations()
{
    clear();
    say "Configurations\n";
    say ". " x 37 . "\n";
    say "1 - Modifier le temps imparti\n";
    print "0 - Quitter\n\n-> ";
    my $saisie = <>;
    chomp $saisie;
    switch ($saisie)
    {
        case 1
        {
            print "Saisissez le nouveau temps imparti (entre 0 et 500 secondes) : ";
            my $nouvTemps = <>;
            chomp $nouvTemps;
            $tempsImparti = $nouvTemps;
        }
        case 0
        {
            return;
        }
    }
}

sub joueTour($$)
{
    my ($joueur, $humain) = @_, my $choix;

    do
    {
        if ($humain == 2 or $humain == 1 && $joueur == 1)
        {
            say "Entrez le numero de la colonne dans laquelle";
            print "vous souhaitez mettre le jeton (entre 1 et 7): ";
            $choix = <>;
            chomp $choix;
            $choix--;# nombre doit etre entre 0 et 6 (indice du tableau)
        }
        else
        {
            $choix = iaRandom();
            print $choix;
        }
    } until (reponsePossible($choix) == 1);

    my $ligne = caseVide($choix);
    $grille[$ligne][$choix] = ($joueur == 0 ? 'O' : 'X');
    return 1 if (victoire($ligne, $choix, $joueur));
    return 0;
}

sub iaRandom()
{
    return int(rand(7));
}

sub coordonneesCorrectes($)
{
    my ($col) = @_;
    return 1 if $col>=0 and $col <= 6;
    return 0;
}

sub caseVide($)
{
    my ($colonne) = @_, my $lig;
    for ($lig = $hauteur - 1 ; $lig >= 0 ; $lig--)
    {
        last if ($grille[$lig][$colonne] eq ' ');
    }
    return $lig if (exists($grille[$lig][$colonne]) && $lig >= 0);
    say "La colonne est remplie !";
    return -1;# case non vide
}

sub reponsePossible($)
{
    my ($choix) = @_;
    return 1 if length $choix == 1 && coordonneesCorrectes($choix) == 1 && caseVide($choix) >= 0;
    return 0;
}

sub afficheGrille($)
{
    my ($joueur) = @_;
    print " ";
    for (my $col = 1 ; $col <= $longueur ; $col++)
    {
        print "  " . $col . " ";
    }
    print "\n ";
    for (my $lig = 0 ; $lig < $hauteur ; $lig++)
    {
        print (("." . " " x 3) x 8);
        print "\n ";
        for (my $col = 0 ; $col < $longueur ; $col++)
        {
            print "| " . BOLD GREEN "O " if ($grille[$lig][$col] eq 'O');
            print "| " . BOLD RED "O " if ($grille[$lig][$col] eq 'X');
            print "|   " if ($grille[$lig][$col] eq ' ');
        }
        print "| \n ";
    }
    print "-" x (($longueur + 1) + $longueur * 3) . "\n\n";
}

sub victoire($$$)
{
    my $compteur = 0, my ($lig, $col, $j) = @_, my $l, my $c;
    for ($l = $hauteur - 1 ; $l >= $lig ; $l--)# on regarde de bas en haut
    {
        $compteur++ if ($grille[$l][$col] eq $J{$j});# un jeton en plus d'affile
        return 1 if ($compteur >= 4);# victoire
        $compteur = 0 unless ($grille[$l][$col] eq $J{$j});
    }

    # on regarde de gauche a droite (toute la ligne)
    for ($c = 0, $compteur = 0 ; $c < $longueur; $c++)
    {
        $compteur++ if ($grille[$lig][$c] eq $J{$j});
        return 1 if ($compteur >= 4);# victoire
        $compteur = 0 unless ($grille[$lig][$c] eq $J{$j});
    }

    $l = $hauteur - 1, $c = $col + ($hauteur - 1 - $lig);
    # diagonale, de bas en haut, de droite a gauche
    for ($compteur = 0 ; $l >= 0 && $c >= 0 && $c < $longueur && $l < $hauteur ; $l--, $c--)
    {
        $compteur++ if ($grille[$l][$c] eq $J{$j});#
        return 1 if ($compteur >= 4);# victoire
        $compteur = 0 unless ($grille[$l][$c] eq $J{$j});#
    }

    # diagonale, de bas en haut, de gauche a droite
    $c = $col - ($hauteur - 1 - $lig), $l = $hauteur -1;
    for ($compteur = 0 ; $l >= 0 && $c < $longueur; $l--, $c++)
    {
        $compteur++ if ($grille[$l][$c] eq $J{$j});
        return 1 if ($compteur >= 4);# victoire
        $compteur = 0 unless ($grille[$l][$c] eq $J{$j});
    }
    return 0;# le jeu continue
}

sub clear()
{
    print "\033[2J";# clear the screen
    print "\033[0;0H";# jump to 0,0
}

sub initialiseGrille()
{
    $row1 = [' ', ' ', ' ', ' ', ' ', ' ', ' '];
    $row2 = [' ', ' ', ' ', ' ', ' ', ' ', ' '];
    $row3 = [' ', ' ', ' ', ' ', ' ', ' ', ' '];
    $row4 = [' ', ' ', ' ', ' ', ' ', ' ', ' '];
    $row5 = [' ', ' ', ' ', ' ', ' ', ' ', ' '];
    $row6 = [' ', ' ', ' ', ' ', ' ', ' ', ' '];
    @grille = ($row1, $row2, $row3, $row4, $row5, $row6);
}


#######################
#                     #
#     APPEL DE LA     #
#    FONCTION MAIN    #
#                     #
#######################


main();
